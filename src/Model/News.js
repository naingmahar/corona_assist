import AsyncStorage from '@react-native-community/async-storage';
import dummyNews from '../dummy/dummyNews';

const NEWS_KEY = '@news';

class News {
  /**
   * @returns {Array<{title, imageUrl, description}>}
   */
  getNews = async () => {
    const json = await AsyncStorage.getItem(NEWS_KEY);
    if (!json) {
      await AsyncStorage.setItem(NEWS_KEY, JSON.stringify(dummyNews));
      return dummyNews;
    }
    return JSON.parse(json);
  };

  setNews = async ({title, imageUrl, description}) => {
    try {
      const currentNews = await this.getNews();
      currentNews.push({
        title,
        imageUrl,
        description,
        key: currentNews.length + '',
      });
      await AsyncStorage.setItem(NEWS_KEY, JSON.stringify(currentNews));
      return {message: 'Success'};
    } catch (error) {
      return {message: error.message};
    }
  };
}

export default new News();
