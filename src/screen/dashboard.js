import React from 'react';
import {View, Image, Dimensions, TouchableOpacity} from 'react-native';
import {
  HeaderBar,
  AppContainer,
  NavContainer,
  BannerContainer,
  RowContainer,
  Container,
  InstructionBox,
} from '../components/container/Container';
import {
  NormalText,
  TitleText,
  SubTitleText,
} from '../components/Typography/Typography';
import {COLOR} from '../theme/color';
import {Button} from '../components/Button/Button';
import {Linking} from 'react-native';
import CoronaMap from './map';
const CallIcon = require('../../assets/call.png');
const SMSIcon = require('../../assets/sms.png');
const SocialDistanceImage = require('../../assets/distance_social.png');
const HandWashImage = require('../../assets/iconfinder_wash_hands_regulary_5964550.png');
const WearingMaskImage = require('../../assets/iconfinder_facial_mask_coronavirus_5964544.jpeg');
const nextImage = require('../../assets/next.png');

const Dashboard = ({navigation, router}) => {
  return (
    <NavContainer navigation={navigation}>
      <BannerContainer>
        <TitleText
          style={{
            fontWeight: 'bold',
            textAlign: 'left',
            marginBottom: 20,
          }}>
          Covid-19
        </TitleText>
        <SubTitleText
          style={{
            fontWeight: 'bold',
            textAlign: 'left',
            color: COLOR.TitleWhiteColor,
            marginBottom: 10,
          }}>
          Are you feeling sick?
        </SubTitleText>

        <NormalText style={{color: COLOR.TitleWhiteColor}}>
          If you feel stick with any of covid-19 symptons please call or SMS us
          immediately for help.
        </NormalText>
        <Container style={{marginTop: 10}}>
          <CoronaMap />
        </Container>
        <RowContainer style={{marginTop: 20}}>
          <Button
            title="Call"
            source={CallIcon}
            style={{marginRight: 20}}
            imageStyle={{width: 28, height: 38, marginRight: 10}}
            onPress={() => Linking.openURL(`tel:${'+959673431072'}`)}
          />
          <Button
            title="SMS"
            source={SMSIcon}
            buttonStyle={{backgroundColor: COLOR.WarningColor}}
            imageStyle={{width: 33, height: 35, marginRight: 10}}
            onPress={() => Linking.openURL(`sms:${'+959673431072'}?body=Hello`)}
          />
        </RowContainer>
      </BannerContainer>

      <TitleText>Prevention</TitleText>

      <RowContainer>
        <Container>
          <Image style={{width: 80, height: 80}} source={SocialDistanceImage} />
          <NormalText style={{color: COLOR.TextColorGray}}>
            Avoid Close content
          </NormalText>
        </Container>

        <Container>
          <Image style={{width: 80, height: 80}} source={HandWashImage} />
          <NormalText style={{color: COLOR.TextColorGray}}>
            {'Clean Your \nHand Often'}
          </NormalText>
        </Container>

        <Container>
          <Image style={{width: 80, height: 80}} source={WearingMaskImage} />
          <NormalText style={{color: COLOR.TextColorGray}}>
            Wear a face mask
          </NormalText>
        </Container>
      </RowContainer>
      <Container>
        <TouchableOpacity
          style={{
            backgroundColor: COLOR.PrimaryColor,
            marginTop: 15,
            flex: 1,
            alignItems: 'center',
            paddingRight: 20,
            paddingLeft: 20,
            borderRadius: 20,
            marginHorizontal: 15,
          }}
          onPress={() => navigation.navigate('Chat')}>
          <Container>
            <InstructionBox />
          </Container>
        </TouchableOpacity>
      </Container>

      <Container>
        <TouchableOpacity
          onPress={() => navigation.navigate('Statistics')}
          style={{
            padding: 15,
            marginHorizontal: 15,
            backgroundColor: COLOR.PrimaryColor,
            width: '94%',
            flexDirection: 'row',
            borderRadius: 20,
          }}>
          <Container>
            <SubTitleText
              style={{
                textAlign: 'left',
                color: COLOR.TextColor,
                fontWeight: 'bold',
              }}>
              See Statistics
            </SubTitleText>
          </Container>

          <Container style={{alignItems: 'flex-end'}}>
            <Image style={{width: 25, height: 25}} source={nextImage} />
          </Container>
        </TouchableOpacity>
      </Container>
    </NavContainer>
  );
};

export default Dashboard;
