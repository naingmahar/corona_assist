import {GiftedChat} from 'react-native-gifted-chat';
import React, {useState, useEffect, useCallback} from 'react';

const botArray = [
  {},
  {},
  {
    _id: 3,
    text: 'Do you have any of the following ?',
    createdAt: new Date(),
    keepIt: true,
    quickReplies: {
      type: 'checkbox',
      values: [
        {
          title: 'Cough',
          value: 'cough',
        },
        {
          title: 'Fever or chills',
          value: 'feverChills',
        },
        {
          title: 'Shortness of breath or breathing difficulty',
          value: 'shortnessOfBreath',
        },
        {
          title: 'Muscle or body aches',
          value: 'bodyAches',
        },
        {
          title: 'Sore Throat',
          value: 'soreThroat',
        },
        {
          title: 'Loss of taste or smell',
          value: 'lossOfTasteOrSmell',
        },
        {
          title: 'Diarrhea',
          value: 'diarrhea',
        },
        {
          title: 'Nausea or vormitting',
          value: 'nauseaVormitting',
        },
        {
          title: 'Fatigue',
          value: 'fatigue',
        },
        {
          title: 'Congestion or running nose',
          value: 'congestion',
        },
        {
          title: 'None of the following',
          value: 'Thank You',
        },
      ],
    },
    user: {
      _id: 2,
      name: 'React Native',
      avatar: 'https://placeimg.com/140/140/any',
    },
  },
  {},
  {
    _id: 5,
    text:
      'Have you had close contact with someone diagnosed with COVID-19 or been notified that you may have been exposed to it?',
    createdAt: new Date(),
    quickReplies: {
      type: 'radio',
      keepIt: true,
      values: [
        {
          title: 'Yes',
          value: 'yes',
        },
        {
          title: 'No',
          value: 'no',
        },
      ],
    },
    user: {
      _id: 2,
      name: 'React Native',
      avatar: 'https://placeimg.com/140/140/any',
    },
  },
  {},
  {
    _id: 7,
    text: 'Thank you for you time! Would you like to restart again ?',
    quickReplies: {
      type: 'radio',
      keepIt: true,
      values: [
        {
          title: 'Yes',
          value: 'yes',
        },
      ],
    },
    createdAt: new Date(),
    user: {
      _id: 2,
      name: 'React Native',
      avatar: 'https://placeimg.com/140/140/any',
    },
  },
];

const chatBot = (length) => {
  return botArray[length];
};
export const Chat = ({route, navigation}) => {
  const params = route.params;
  const [messages, setMessages] = useState([]);
  const initialMessage = {
    _id: 1,
    text:
      'Welcome to Our Coronavirus Self-Checker. If you are experiencing a life-threatening emergency, such as severe shortness of breath or high fever, call +959673431072. This self-checker is not a substitute for professional medical advice, diagnosis, or treatment. Always consult a medical professional for serious symptoms or emergencies.',
    createdAt: new Date(),
    quickReplies: {
      type: 'radio',
      keepIt: true,
      values: [
        {
          title: 'I Accept',
          value: 'yes',
        },
      ],
    },
    user: {
      _id: 2,
      name: 'React Native',
      avatar: 'https://placeimg.com/140/140/any',
    },
  };

  useEffect(() => {
    setMessages([initialMessage]);
  }, []);

  const onSend = useCallback((message = []) => {
    setMessages((previousMessages) =>
      GiftedChat.append(previousMessages, message),
    );
    setMessages((previousMessages) =>
      GiftedChat.append(previousMessages, chatBot(previousMessages.length)),
    );
  }, []);

  const onQuickReply = (replies) => {
    const createdAt = new Date();
    if (replies.length === 1) {
      if (
        replies[0].messageId === 3 &&
        replies[0].title === 'None of the following'
      ) {
        onSend([
          {
            createdAt,
            _id: Math.round(Math.random() * 1000000),
            text: replies[0].value,
            user: {
              _id: 2,
              name: 'React Native',
              avatar: 'https://placeimg.com/140/140/any',
            },
          },
        ]);
      } else if (replies[0].messageId === 5 && replies[0].title === 'No') {
        onSend([
          {
            createdAt,
            _id: Math.round(Math.random() * 1000000),
            text: `Seems like you don't have positive signs of COVID-19. But you need to check with medical specialists or with professionals.`,
            user: {
              _id: 2,
              name: 'React Native',
              avatar: 'https://placeimg.com/140/140/any',
            },
          },
        ]);
      } else if (replies[0].messageId === 5 && replies[0].title === 'Yes') {
        onSend([
          {
            _id: Math.round(Math.random() * 100000),
            text:
              'Your answers indicate that you should call your health care provider and describe your symptoms and/or your contact with someone who’s been diagnosed. Be sure to indicate if you: Are 65 or older. Suffer from a chronic health problem, such as diabetes, heart disease, cancer or lung disease. A health care or emergency worker who has been interacting with patients who may have COVID-19. Are immunocompromised, such as people with HIV/AIDS and cancer.Are pregnant, or ad a babyh within the last 2 weeks.Your doctor will determine next steps.Most COVID-19 cases can be managed at home. Severe symptoms may require hospital treatment.',
            createdAt: new Date(),
            user: {
              _id: 2,
              name: 'React Native',
              avatar: 'https://placeimg.com/140/140/any',
            },
          },
        ]);
      } else if (
        replies[0].messageId === 9 ||
        (replies[0].messageId === 7 && replies[0].title === 'Yes')
      ) {
        setMessages([initialMessage]);
      } else {
        onSend([
          {
            createdAt,
            _id: Math.round(Math.random() * 1000000),
            text: replies[0].title,
            user: {
              _id: 2,
              name: 'React Native',
              avatar: 'https://placeimg.com/140/140/any',
            },
          },
        ]);
      }
    } else if (replies.length > 1) {
      onSend([
        {
          createdAt,
          _id: Math.round(Math.random() * 1000000),
          text: replies.map((reply) => reply.title).join(', '),
          user: {
            _id: 2,
            name: 'React Native',
            avatar: 'https://placeimg.com/140/140/any',
          },
        },
      ]);
    } else {
      console.warn('replies param is not set correctly');
    }
  };
  return (
    <GiftedChat
      messages={messages}
      onSend={(messages) => onSend(messages)}
      onQuickReply={onQuickReply}
      user={{
        _id: 1,
      }}
    />
  );
};
