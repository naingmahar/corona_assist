import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  TextInput,
  AsyncStorage,
  Keyboard,
} from 'react-native';

import {
  AppContainer,
  Container,
  RowContainer,
  FormContainer,
} from '../components/container/Container';

import {
  TitleText,
  MainTitleText,
  SubTitleText,
  NormalText,
} from '../components/Typography/Typography';

import {TextBox} from '../TextBox/TextBox';
import {LinkButton, Button} from '../components/Button/Button';
import User from '../Model/User';
const CoronaImage = require('../../assets/coronavirus.png');

const Register = ({navigation, router}) => {
  const [username, setUserName] = useState('');
  const [name, setName] = useState('');
  const [age, setAge] = useState('');
  const [phone, setPhone] = useState('');
  const [password, setPassword] = useState('');

  const onsubmit = async () => {
    Keyboard.dismiss();
    const {message} = await User.register({
      username,
      password,
      age,
      name,
      phone,
    });
    alert(message);
  };

  return (
    <>
      <FormContainer>
        <Container>
          <MainTitleText>Corona Assist</MainTitleText>
          <TitleText style={{textAlign: 'right'}}>By HAL</TitleText>
        </Container>
        <Container>
          <Image source={CoronaImage} />
        </Container>
        <TitleText>Register</TitleText>
        <Container style={{flex: 2}}>
          <TextBox placeholder="Email" onChange={(text) => setUserName(text)} />

          <TextBox
            placeholder="Password"
            secureTextEntry={true}
            onChange={(text) => {
              setPassword(text);
            }}
          />

          <TextBox placeholder="Name" onChange={(text) => setName(text)} />
          <TextBox placeholder="Phone" onChange={(text) => setPhone(text)} />
          <TextBox placeholder="Age" onChange={(text) => setAge(text)} />
        </Container>
        <Button title="Register" onPress={onsubmit}></Button>
        <LinkButton
          title="Do you want to Login?"
          onPress={() => navigation.navigate('Login')}
        />
      </FormContainer>
    </>
  );
};

export default Register;
