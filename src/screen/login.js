import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  TextInput,
  Keyboard,
} from 'react-native';

import {
  AppContainer,
  Container,
  RowContainer,
  FormContainer,
} from '../components/container/Container';

import {
  TitleText,
  MainTitleText,
  SubTitleText,
  NormalText,
} from '../components/Typography/Typography';

import {TextBox} from '../TextBox/TextBox';
import {LinkButton, Button} from '../components/Button/Button';
import User from '../Model/User';

const CoronaImage = require('../../assets/coronavirus.png');
const GoogleImage = require('../../assets/google-logo.png');
const FaceBookImage = require('../../assets/facebook-logo.png');

const Login = ({navigation, router}) => {
  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');

  const onsubmit = async () => {
    Keyboard.dismiss();
    const {message, userIndex} = await User.validate({username, password});
    alert(message);
    if (userIndex >= 0) navigation.navigate('Home');
  };

  return (
    <>
      <FormContainer>
        <Container style={{marginTop: 30, marginBottom: 20}}>
          <MainTitleText>Corona Assist</MainTitleText>
          <TitleText style={{textAlign: 'right'}}>By HAL</TitleText>
        </Container>
        <Container>
          <Image source={CoronaImage} />
        </Container>
        <Container>
          <TitleText style={{marginTop: 20, marginBottom: 20}}>LOGIN</TitleText>
        </Container>
        <Container>
          <TextBox
            placeholder="User Name"
            onChange={(text) => setUserName(text)}
          />
          <TextBox
            placeholder="Password"
            secureTextEntry={true}
            onChange={(text) => {
              setPassword(text);
            }}
            onSubmitEditing={onsubmit}
          />
          <Button title="Login" onPress={onsubmit} />
        </Container>
        <LinkButton
          title="Do you want to Register?"
          onPress={() => navigation.navigate('Register')}
        />
      </FormContainer>
    </>
  );
};

export default Login;
