import axios from 'axios';

export const getTotalCovidData = async () => {
  try {
    const response = await axios.get(
      'https://corona-stats.online/MM?format=json',
    );
    return response.data;
  } catch (error) {
    return error;
  }
};
