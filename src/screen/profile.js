import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  TextInput,
  AsyncStorage,
  Keyboard,
} from 'react-native';

import {
  AppContainer,
  Container,
  RowContainer,
  HeaderBar,
} from '../components/container/Container';

import {
  TitleText,
  MainTitleText,
  SubTitleText,
  NormalText,
} from '../components/Typography/Typography';

import {TextBox} from '../TextBox/TextBox';
import {LinkButton} from '../components/Button/Button';
import User from '../Model/User';
const UserImage = require('../../assets/user.webp');

const Profile = ({navigation, router}) => {
  const [user, setUser] = useState({
    username: '',
    name: '',
    phone: '',
    age: '',
  });

  useEffect(() => {
    setUserData();
  }, []);

  const setUserData = async () => {
    const usr = await User.getUser();
    // console.log('Data', usr);
    setUser(usr);
  };

  return (
    <>
      <HeaderBar navigation={navigation} />
      <AppContainer>
        <Container />
        <Container>
          <Image source={UserImage} style={{width: 200, height: 200}} />
        </Container>
        <Container>
          <RowContainer />
          <RowContainer>
            <TitleText bold>Username: </TitleText>
            <TitleText>{user.username}</TitleText>
          </RowContainer>
          <RowContainer>
            <TitleText bold>Name: </TitleText>
            <TitleText>{user.name}</TitleText>
          </RowContainer>
          <RowContainer>
            <TitleText bold>Phone: </TitleText>
            <TitleText>{user.phone}</TitleText>
          </RowContainer>
          <RowContainer>
            <TitleText bold>Age: </TitleText>
            <TitleText>{user.age}</TitleText>
          </RowContainer>

          <RowContainer />
        </Container>

        <Container />
      </AppContainer>
    </>
  );
};

export default Profile;
