import React from 'react';
import Video from 'react-native-video-controls';
import {StyleSheet} from 'react-native';
import {Container} from '../../components/container/Container';

export const VideoPlayer = ({route, navigation}) => {
  const {params} = route;
  return (
    <Container>
      <Video
        source={params.source}
        navigator={navigation}
        controlAnimationTiming={500}
        toggleResizeModeOnFullscreen
        style={styles.backgroundVideo}
      />
    </Container>
  );
};

var styles = StyleSheet.create({
  backgroundVideo: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});
