import React from 'react';
import {View, SafeAreaView, Dimensions, Image} from 'react-native';
import {COLOR} from '../../theme/color';
import {SubTitleText, NormalText, TitleText} from '../Typography/Typography';
import {TouchableOpacity, ScrollView} from 'react-native-gesture-handler';

const notiImage = require('../../../assets/logout.png');
const navImage = require('../../../assets/nav.png');
const homeImage = require('../../../assets/home.png');
const ownTestImage = require('../../../assets/own_test.png');

export const AppContainer = ({children, style}) => {
  return (
    <SafeAreaView
      style={[
        {
          flex: 1,
          display: 'flex',
          justifyContent: 'center',
          backgroundColor: COLOR.PrimaryColor,
          padding: 20,
        },
        style,
      ]}>
      {children}
    </SafeAreaView>
  );
};

export const FormContainer = ({children, style}) => {
  return (
    <SafeAreaView
      style={[
        {
          flex: 1,
          display: 'flex',
          justifyContent: 'center',
          backgroundColor: COLOR.PrimaryColor,
          padding: 20,
        },
        style,
      ]}>
      <ScrollView style={{flex: 1}}>{children}</ScrollView>
    </SafeAreaView>
  );
};

export const Container = ({children, style}) => {
  return (
    <View
      style={[
        {
          flex: 1,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
        },
        style,
      ]}>
      {children}
    </View>
  );
};

export const RowContainer = ({children, style}) => {
  return (
    <View
      style={[
        {
          flex: 1,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'row',
        },
        style,
      ]}>
      {children}
    </View>
  );
};

export const HeaderBar = ({navigation}) => {
  return (
    <View
      style={{
        backgroundColor: COLOR.PrimaryColor,
        height: 40,
        width: Dimensions.get('screen').width,
        alignItems: 'center',
        paddingRight: 20,
        paddingLeft: 20,
      }}>
      <RowContainer>
        <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
          <Image
            style={{width: 25, height: 25, marginRight: 20}}
            source={navImage}
          />
        </TouchableOpacity>
        <RowContainer style={{justifyContent: 'flex-start'}}>
          <TitleText style={{color: COLOR.TitleWhiteColor}}>
            {'Corona Assist '}
          </TitleText>
          <NormalText style={{color: COLOR.TitleWhiteColor}}>by HAL</NormalText>
        </RowContainer>
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Image
            style={{width: 25, height: 25, justifyContent: 'flex-end'}}
            source={notiImage}
          />
        </TouchableOpacity>
      </RowContainer>
    </View>
  );
};

export const MenuBar = ({navigation}) => {
  return (
    <View
      style={{
        backgroundColor: COLOR.MenuBarColor,
        height: 60,
        width: Dimensions.get('screen').width,
        alignItems: 'center',
        paddingRight: 20,
        paddingLeft: 20,
      }}>
      <RowContainer style={{justifyContent: 'flex-start'}}>
        <Container style={{paddingRight: 10}}>
          <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
            <Image style={{width: 25, height: 25}} source={homeImage} />
          </TouchableOpacity>
        </Container>

        <Container style={{flex: 4}}>
          <TouchableOpacity
            onPress={() => navigation.navigate('Entertainment')}>
            <SubTitleText style={{color: COLOR.TitleWhiteColor}}>
              Entertainment
            </SubTitleText>
          </TouchableOpacity>
        </Container>

        <Container style={{flex: 3}}>
          <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
            <SubTitleText style={{color: COLOR.TitleWhiteColor}}>
              Profile
            </SubTitleText>
          </TouchableOpacity>
        </Container>

        <Container style={{flex: 3}}>
          <TouchableOpacity onPress={() => navigation.navigate('News')}>
            <SubTitleText style={{color: COLOR.TitleWhiteColor}}>
              News
            </SubTitleText>
          </TouchableOpacity>
        </Container>
      </RowContainer>
    </View>
  );
};

export const BannerContainer = ({style, children}) => {
  return (
    <View
      style={{
        height: '40%',
        backgroundColor: COLOR.PrimaryColor,
        borderBottomLeftRadius: 50,
        borderBottomRightRadius: 50,
        padding: 20,
      }}>
      {children}
    </View>
  );
};

export const NavContainer = ({style, children, active, navigation}) => (
  <>
    <HeaderBar navigation={navigation} />
    <MenuBar navigation={navigation} />
    <AppContainer
      style={{
        backgroundColor: COLOR.BackgroundColor,
        justifyContent: 'flex-start',
        padding: 0,
      }}>
      {children}
    </AppContainer>
  </>
);

export const InstructionBox = () => {
  return (
    <View
      style={{
        backgroundColor: COLOR.PrimaryColor,
        flex: 1,
        alignItems: 'center',
        paddingRight: 20,
        paddingLeft: 20,
        borderRadius: 20,
        marginHorizontal: 15,
      }}>
      <RowContainer>
        <Container>
          <Image style={{width: 80, height: 80}} source={ownTestImage} />
        </Container>
        <Container style={{flex: 2}}>
          <TitleText style={{color: COLOR.TitleWhiteColor, fontWeight: 'bold'}}>
            {'Do you own test!'}
          </TitleText>

          <NormalText style={{color: COLOR.TitleWhiteColor, textAlign: 'left'}}>
            {'Follow the instructions to do your own test.'}
          </NormalText>
        </Container>
      </RowContainer>
    </View>
  );
};

export const ColorBox = ({backgroundColor, title, text}) => {
  return (
    <View
      style={{
        backgroundColor,
        padding: 20,
        borderRadius: 20,
        flex: 1,
        marginHorizontal: 5,
        alignItems: 'center',
      }}>
      <NormalText isWhite bold>
        {title}
      </NormalText>
      <NormalText isWhite bold>
        {text}
      </NormalText>
    </View>
  );
};
