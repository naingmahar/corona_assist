import React from 'react';
import {View, Image, TouchableOpacity} from 'react-native';
import {NormalText, SubTitleText} from '../Typography/Typography';
import {COLOR} from '../../theme/color';
import {RowContainer} from '../container/Container';
import {FontSize} from '../../theme/fontSize';

export const LinkButton = ({title, style, onPress, isSmall}) => {
  return (
    <View style={[{justifyContent: 'center'}]}>
      <TouchableOpacity
        onPress={() => onPress()}
        style={{
          paddingLeft: 20,
          paddingRight: 20,
          paddingTop: 10,
          paddingBottom: 10,
        }}>
        <SubTitleText
          style={{
            fontSize: isSmall ? FontSize.NormalTextSize : FontSize.SubTitleSize,
            color: COLOR.TitleWhiteColor,
            textDecorationLine: 'underline',
          }}>
          {title}
        </SubTitleText>
      </TouchableOpacity>
    </View>
  );
};

export const Button = ({
  title,
  style,
  buttonStyle,
  onPress,
  source,
  imageStyle,
  textStyle,
}) => {
  return (
    <View style={[{justifyContent: 'center'}, style]}>
      <TouchableOpacity
        onPress={() => onPress()}
        style={[
          {
            paddingLeft: 40,
            paddingRight: 40,
            paddingTop: 30,
            paddingBottom: 30,
            backgroundColor: COLOR.SuccessColor,
            borderRadius: 40,
            height: 40,
            justifyContent: 'center',

            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.22,
            shadowRadius: 2.22,

            elevation: 3,
          },
          buttonStyle,
        ]}>
        <RowContainer>
          <Image source={source} style={imageStyle} />
          <SubTitleText
            style={[
              {
                color: COLOR.TitleWhiteColor,
              },
              textStyle,
            ]}>
            {title}
          </SubTitleText>
        </RowContainer>
      </TouchableOpacity>
    </View>
  );
};
