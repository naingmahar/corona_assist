import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  TextInput,
  Keyboard,
} from 'react-native';

import {
  AppContainer,
  Container,
  RowContainer,
  FormContainer,
} from '../components/container/Container';

import {
  TitleText,
  MainTitleText,
  SubTitleText,
  NormalText,
} from '../components/Typography/Typography';

import {TextBox} from '../TextBox/TextBox';
import {LinkButton} from '../components/Button/Button';
import User from '../Model/User';

const CoronaImage = require('../../assets/coronavirus.png');
const GoogleImage = require('../../assets/google-logo.png');
const FaceBookImage = require('../../assets/facebook-logo.png');

const Welcome = ({navigation, router}) => {
  useEffect(() => {
    const checkLogin = async () => {
      const data = await User.checkAlreadyLogin();
      if (data) {
        navigation.navigate('Home');
        return;
      } else {
        navigation.navigate('Login');
        return;
      }
    };

    checkLogin();
  });
  return (
    <>
      <AppContainer>
        <Container />
        <Container />
        <Container>
          <Image source={CoronaImage} />
        </Container>
        <Container>
          <MainTitleText>Corona Assist</MainTitleText>
          <TitleText>By HAL</TitleText>
        </Container>
        <Container />
        <Container />
      </AppContainer>
    </>
  );
};

export default Welcome;
