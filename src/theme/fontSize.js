export const FontSize = {
  MainTitle: 32,
  TitleSize: 20,
  SubTitleSize: 18,
  NormalTextSize: 14,
  SmallTextSize: 12,
};
