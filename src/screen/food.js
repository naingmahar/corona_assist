import React from 'react';
import {View, Image, TouchableOpacity, Text} from 'react-native';
import {
  AppContainer,
  RowContainer,
  NavContainer,
  Container,
} from '../components/container/Container';
import {
  TitleText,
  NormalText,
  SmallText,
  VideoText,
} from '../components/Typography/Typography';
import {COLOR} from '../theme/color';

const playerImg = require('../../assets/player.png');

const VideoSource1 = {
  uri:
    'https://naingmahar.s3-ap-southeast-1.amazonaws.com/COVID_19___Best_Food_Rich_In_Vitamin_C%2C_Vitamin_D_and_Zinc%2C_Should_you_take_Vitamin_D_and_Vitamin_C_(720p).mp4',
};

const TempSource1 = require('../../assets/CovidFood.png');

const VideoSource2 = {
  uri:
    'https://naingmahar.s3-ap-southeast-1.amazonaws.com/Healthy_Living__Boosting_Your_Immune_System_for_Covid-19(480p).mp4',
};
const TempSource2 = require('../../assets/CoronaLifestyle.jpg');

const Card = ({children, onPress}) => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: 'red',
        // shadowColor: '#000',
        // shadowOffset: {
        //   width: 0,
        //   height: 2,
        // },
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,

        // elevation: 5,
      }}>
      <TouchableOpacity onPress={() => onPress()}>{children}</TouchableOpacity>
    </View>
  );
};
export const Food = ({navigation}) => {
  const onPress = (source) => {
    navigation.navigate('VideoPlayer', {source});
  };

  return (
    <NavContainer navigation={navigation}>
      <AppContainer>
        <Container />
        <RowContainer style={{flex: 1}}>
          <Container>
            <Card onPress={() => onPress(VideoSource1)}>
              <Image
                source={TempSource1}
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              />
              <Image
                source={playerImg}
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 10,
                  position: 'absolute',
                  backgroundColor: '#00000070',
                }}
              />
            </Card>
            <VideoText>{'Best food rich in Vitamin C, D and Zinc'}</VideoText>
          </Container>

          <Container>
            <Card onPress={() => onPress(VideoSource2)}>
              <Image
                source={TempSource2}
                style={{width: 100, height: 100, borderRadius: 10}}
              />
              <Image
                source={playerImg}
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 10,
                  position: 'absolute',
                  backgroundColor: '#00000070',
                }}
              />
            </Card>

            <VideoText>
              {'Healthy lifstyle to boost your Immune System'}
            </VideoText>
          </Container>
        </RowContainer>
        <Container />
        <Container />
      </AppContainer>
    </NavContainer>
  );
};
