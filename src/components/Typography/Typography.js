import React from 'react';
import {Text} from 'react-native';
import {FontSize} from '../../theme/fontSize';
import {COLOR} from '../../theme/color';

export const TitleText = ({children, style, bold, isGray}) => {
  return (
    <Text
      style={[
        {
          fontSize: FontSize.TitleSize,
          color: COLOR.TitleWhiteColor,
          textAlign: 'center',
          fontWeight: bold ? 'bold' : 'normal',
          color: isGray ? COLOR.TextColorGray : COLOR.TextColor,
        },
        style,
      ]}>
      {children}
    </Text>
  );
};

export const MainTitleText = ({children, style}) => {
  return (
    <Text
      style={[
        {
          fontSize: FontSize.MainTitle,
          color: COLOR.TitleWhiteColor,
          textAlign: 'center',
          fontWeight: 'bold',
        },
        style,
      ]}>
      {children}
    </Text>
  );
};

export const SubTitleText = ({children, style, bold, isWhite}) => {
  return (
    <Text
      style={[
        {
          fontSize: FontSize.SubTitleSize,
          color: COLOR.TitleColor,
          textAlign: 'center',
          fontWeight: bold ? 'bold' : 'normal',
          color: isWhite ? COLOR.TextColor : COLOR.TextColorGray,
        },
        style,
      ]}>
      {children}
    </Text>
  );
};

export const NormalText = ({children, style, bold, isWhite}) => {
  return (
    <Text
      style={[
        {
          fontSize: FontSize.NormalTextSize,
          color: COLOR.TextColor,
          fontWeight: bold ? 'bold' : 'normal',
          color: isWhite ? COLOR.TextColor : COLOR.TextColorGray,
        },
        style,
      ]}>
      {children}
    </Text>
  );
};

export const SmallText = ({children, style}) => {
  return (
    <Text
      style={[
        {
          fontSize: FontSize.SmallTextSize,
          color: COLOR.TextColor,
        },
        style,
      ]}>
      {children}
    </Text>
  );
};

export const VideoText = ({children, style}) => {
  return (
    <SmallText
      style={{
        textAlign: 'center',
        color: 'black',
        textDecorationLine: 'underline',
        fontWeight: 'bold',
      }}>
      {children}
    </SmallText>
  );
};
