import React from 'react';
import {View, Image, TouchableOpacity, Text} from 'react-native';
import {
  AppContainer,
  RowContainer,
  NavContainer,
  Container,
} from '../components/container/Container';
import {
  TitleText,
  NormalText,
  SmallText,
  VideoText,
} from '../components/Typography/Typography';
import {COLOR} from '../theme/color';

const playerImg = require('../../assets/player.png');

const VideoSource1 = {
  uri:
    'https://naingmahar.s3-ap-southeast-1.amazonaws.com/assets_ent_video_1.mp4',
};

const TempSource1 = require('../../assets/video_img/1.jpg');

const VideoSource2 = {
  uri:
    'https://naingmahar.s3-ap-southeast-1.amazonaws.com/assets_ent_video_2.mp4',
};
const TempSource2 = require('../../assets/video_img/2.jpg');

const VideoSource3 = {
  uri:
    'https://naingmahar.s3-ap-southeast-1.amazonaws.com/assets_ent_video_3.mp4',
};
const TempSource3 = require('../../assets/video_img/3.jpg');

const VideoSource4 = {
  uri:
    'https://naingmahar.s3-ap-southeast-1.amazonaws.com/assets_ent_video_4.mp4',
};
const TempSource4 = require('../../assets/video_img/4.jpg');

const VideoSource5 = {
  uri:
    'https://naingmahar.s3-ap-southeast-1.amazonaws.com/assets_ent_video_5.mp4',
};
const TempSource5 = require('../../assets/video_img/5.jpg');

const Card = ({children, onPress}) => {
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: 'red',
        // shadowColor: '#000',
        // shadowOffset: {
        //   width: 0,
        //   height: 2,
        // },
        // shadowOpacity: 0.25,
        // shadowRadius: 3.84,

        // elevation: 5,
      }}>
      <TouchableOpacity onPress={() => onPress()}>{children}</TouchableOpacity>
    </View>
  );
};
export const Entertainment = ({navigation}) => {
  const onPress = (source) => {
    navigation.navigate('VideoPlayer', {source});
  };

  return (
    <NavContainer navigation={navigation}>
      <AppContainer>
        <Container />
        <RowContainer style={{flex: 2}}>
          <Container>
            <Card onPress={() => onPress(VideoSource1)}>
              <Image
                source={TempSource1}
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              />
              <Image
                source={playerImg}
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 10,
                  position: 'absolute',
                  backgroundColor: '#00000070',
                }}
              />
            </Card>
            <VideoText>{'This is a great adventure \n for you'}</VideoText>
            <VideoText>{'Cast: Dr. Laurel'}</VideoText>
          </Container>

          <Container>
            <Card onPress={() => onPress(VideoSource2)}>
              <Image
                source={TempSource2}
                style={{width: 100, height: 100, borderRadius: 10}}
              />
              <Image
                source={playerImg}
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 10,
                  position: 'absolute',
                  backgroundColor: '#00000070',
                }}
              />
            </Card>

            <VideoText>{'The Pain Killers of Corona'}</VideoText>
            <VideoText>{'Cast: Seven Sense Production'}</VideoText>
          </Container>
        </RowContainer>
        <RowContainer style={{flex: 2}}>
          <Container>
            <Card onPress={() => onPress(VideoSource4)}>
              <Image
                source={TempSource4}
                style={{width: 100, height: 100, borderRadius: 10}}
              />
              <Image
                source={playerImg}
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 10,
                  position: 'absolute',
                  backgroundColor: '#00000070',
                }}
              />
            </Card>
            <VideoText>{'Funny videos'}</VideoText>
            <VideoText>{'Cast: Zarni Htut Tin'}</VideoText>
          </Container>

          <Container>
            <Card onPress={() => onPress(VideoSource5)}>
              <Image
                source={TempSource5}
                style={{width: 100, height: 100, borderRadius: 10}}
              />
              <Image
                source={playerImg}
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 10,
                  position: 'absolute',
                  backgroundColor: '#00000070',
                }}
              />
            </Card>
            <VideoText>{'Funny And Entertainment'}</VideoText>
            <VideoText>{'Cast: Fool Ever Young'}</VideoText>
          </Container>
        </RowContainer>
        <RowContainer style={{flex: 2}}>
          <Container>
            <Card onPress={() => onPress(VideoSource3)}>
              <Image
                source={TempSource3}
                style={{width: 100, height: 100, borderRadius: 10}}
              />
              <Image
                source={playerImg}
                style={{
                  width: 100,
                  height: 100,
                  borderRadius: 10,
                  position: 'absolute',
                  backgroundColor: '#00000070',
                }}
              />
            </Card>
            <VideoText>{'Go Away Corona'}</VideoText>
            <VideoText>{'Cast: MOHS'}</VideoText>
          </Container>
        </RowContainer>
        <Container />
      </AppContainer>
    </NavContainer>
  );
};
