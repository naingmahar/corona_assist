import React from 'react';
import {TextInput} from 'react-native';

export const TextBox = ({
  style,
  placeholder,
  value,
  onChange,
  secureTextEntry,
  onSubmitEditing,
  multiline,
  numberOfLines,
}) => {
  return (
    <TextInput
      onChangeText={(text) => onChange(text)}
      autoCapitalize={'none'}
      style={[
        {
          minHeight: 40,
          width: '100%',
          borderColor: 'white',
          borderWidth: 1,
          borderRadius: 5,
          backgroundColor: 'white',
          paddingLeft: 20,
          marginBottom: 10,
        },
        style,
      ]}
      secureTextEntry={secureTextEntry}
      placeholder={placeholder}
      value={value}
      multiline={multiline ? multiline : false}
      numberOfLines={numberOfLines ? numberOfLines : 1}
      onSubmitEditing={() =>
        onSubmitEditing ? onSubmitEditing() : console.log()
      }
    />
  );
};
