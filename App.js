import React, {useState} from 'react';
import {StatusBar} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {COLOR} from './src/theme/color';
import Login from './src/screen/login';
import {NavigationContainer} from '@react-navigation/native';
import Register from './src/screen/register';
import Dashboard from './src/screen/dashboard';
import Statistics from './src/screen/statistics';
import {Entertainment} from './src/screen/entertainment';
import {VideoPlayer} from './src/screen/Video/player';
import {Chat} from './src/screen/chat';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Profile from './src/screen/profile';
import News from './src/screen/news';
import Welcome from './src/screen/welcome';
import {AddNews} from './src/screen/AddNews';
import {Food} from './src/screen/food';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

console.warn = () => {};
const MainNav = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Dashboard" component={Dashboard} />
      <Stack.Screen name="Statistics" component={Statistics} />
      <Stack.Screen name="Entertainment" component={Entertainment} />
      <Stack.Screen name="Food" component={Food} />
      <Stack.Screen name="News" component={News} />
    </Stack.Navigator>
  );
};

const DrawerNav = () => {
  return (
    <Drawer.Navigator initialRouteName="Home">
      <Drawer.Screen name="Home" children={MainNav} />
      <Drawer.Screen name="Profile" component={Profile} />
      <Drawer.Screen name="Statistics" component={Statistics} />
      <Drawer.Screen name="Entertainment" component={Entertainment} />
      <Drawer.Screen name="News" component={News} />
      <Drawer.Screen name="Food" component={Food} />
      <Drawer.Screen name="Covid Test" component={Chat} />
    </Drawer.Navigator>
  );
};
const App = () => {
  return (
    <>
      <StatusBar backgroundColor={COLOR.PrimaryColor} barStyle="dark-content" />
      <NavigationContainer>
        <Stack.Navigator screenOptions={{headerShown: false}} mode="modal">
          <Stack.Screen name="Welcome" component={Welcome} />
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="Home" children={DrawerNav} />
          <Stack.Screen name="Chat" component={Chat} />
          <Stack.Screen name="VideoPlayer" component={VideoPlayer} />
          <Stack.Screen name="AddNews" component={AddNews} />
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default App;
