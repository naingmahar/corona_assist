import AsyncStorage from '@react-native-community/async-storage';

const USER_KEY = '@user';
const LOGIN_KEY = '@login';

class User {
  constructor() {
    this.isLogin = false;
    this.userIndex = null;
    this.username = null;
  }

  getUser = async () => {
    if (!this.userIndex < 0) return;
    try {
      let users = await AsyncStorage.getItem(USER_KEY);
      users = JSON.parse(users);
      return users[this.userIndex];
    } catch (error) {
      // console.log('Error', error);
      return;
    }
  };

  logout = async () => {
    await AsyncStorage.removeItem(LOGIN_KEY);
  };

  checkAlreadyLogin = async () => {
    const loginJson = await AsyncStorage.getItem(LOGIN_KEY);
    if (loginJson) {
      let {username, userIndex} = JSON.parse(loginJson);
      this.isLogin = true;
      this.userIndex = userIndex;
      this.username = username;
      return {message: 'Success login!', userIndex};
    }

    return null;
  };

  validate = async ({username, password}) => {
    // AsyncStorage.removeItem(USER_KEY);
    let oldUsersRaw = await AsyncStorage.getItem(USER_KEY);
    if (!oldUsersRaw) {
      await this.register(
        {
          username: 'hal',
          password: '123',
          age: 21,
          name: 'HAL',
          phone: '09444444055',
        },
        true,
      );
      oldUsersRaw = await AsyncStorage.getItem(USER_KEY);
    }

    let users = [{username: '', password: ''}];
    let userIndex = -1;
    if (oldUsersRaw) users = JSON.parse(oldUsersRaw);
    userIndex = users.findIndex(
      (user) => user.username === username && user.password === password,
    );
    if (userIndex < 0) return {message: 'Invalid username or password'};
    this.isLogin = true;
    this.userIndex = userIndex;
    this.username = username;

    await AsyncStorage.setItem(
      LOGIN_KEY,
      JSON.stringify({username, password, userIndex}),
    );
    return {message: 'Success login!', userIndex};
  };

  register = async ({username, password, age, name, phone}, admin = false) => {
    try {
      let oldUsersRaw = await AsyncStorage.getItem(USER_KEY);
      let users = [];
      if (oldUsersRaw) users = JSON.parse(oldUsersRaw);
      users.push({username, password, age, name, phone, admin});
      await AsyncStorage.setItem(USER_KEY, JSON.stringify(users));
      return {message: 'Success'};
    } catch (error) {
      // console.log(error);
      return {message: 'Register fail'};
    }
  };
}

export default new User();
