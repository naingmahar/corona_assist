import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  TextInput,
  AsyncStorage,
  Keyboard,
} from 'react-native';

import {
  AppContainer,
  Container,
  NavContainer,
  RowContainer,
} from '../components/container/Container';

import {
  TitleText,
  MainTitleText,
  SubTitleText,
  NormalText,
} from '../components/Typography/Typography';

import {TextBox} from '../TextBox/TextBox';
import {LinkButton, Button} from '../components/Button/Button';
import User from '../Model/User';
import {} from 'react-native-view-more-text';
import {FlatList} from 'react-native-gesture-handler';
const UserImage = require('../../assets/user.webp');
import ViewMoreText from 'react-native-view-more-text';
import dummyNews from '../dummy/dummyNews';
import NewsModel from '../Model/News';
import {COLOR} from '../theme/color';
const Card = ({title, description, imageUrl}) => {
  let renderViewMore = (onPress) => {
    return <Text onPress={onPress}>View more</Text>;
  };

  let renderViewLess = (onPress) => {
    return <Text onPress={onPress}>View less</Text>;
  };

  return (
    <Container
      style={{
        // borderWidth: 1,
        padding: 20,
        marginBottom: 10,
        borderRadius: 10,
        backgroundColor: '#fff',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,

        elevation: 3,
      }}>
      <View style={{}} />
      <Image
        source={{
          uri: imageUrl,
        }}
        style={{width: '50%', height: 150}}
      />
      <SubTitleText style={{marginTop: 10, marginBottom: 10}}>
        {title}
      </SubTitleText>
      <ViewMoreText
        numberOfLines={3}
        renderViewMore={renderViewMore}
        renderViewLess={renderViewLess}
        textStyle={{textAlign: 'center'}}>
        <NormalText>{description}</NormalText>
      </ViewMoreText>
    </Container>
  );
};

const News = ({navigation, router}) => {
  const [news, setNews] = useState([]);
  const [admin, setAdmin] = useState(false);

  useEffect(() => {
    navigation.addListener('focus', () => {
      setData();
      getUser();
    });
  }, []);

  const setData = async () => {
    const data = await NewsModel.getNews();

    setNews(data.reverse());
  };

  const getUser = async () => {
    const user = await User.getUser();
    setAdmin(user.admin ? true : false);
  };

  return (
    <>
      <NavContainer navigation={navigation} style={{backgroundColor: 'gray'}}>
        <AppContainer>
          {admin && (
            <Button
              title="+ Add News"
              style={{marginBottom: 20}}
              buttonStyle={{backgroundColor: COLOR.PrimaryColor}}
              onPress={() => navigation.navigate('AddNews')}
            />
          )}
          <FlatList
            style={{flex: 4}}
            data={news}
            renderItem={({item}) => (
              <Card
                title={item.title}
                imageUrl={item.imageUrl}
                description={item.description}
              />
            )}
            keyExtractor={(item, index) => index + ''}
          />
        </AppContainer>
      </NavContainer>
    </>
  );
};

export default News;
