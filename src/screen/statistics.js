import React, {useState, useEffect} from 'react';
import {Button, LinkButton} from '../components/Button/Button';
import {
  NavContainer,
  RowContainer,
  Container,
  AppContainer,
  ColorBox,
} from '../components/container/Container';
import {COLOR} from '../theme/color';
import App from '../../App';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {NormalText, SubTitleText} from '../components/Typography/Typography';
import {Image, View, Dimensions} from 'react-native';
import {getTotalCovidData} from '../lib/api/api';
import {color} from 'react-native-reanimated';

const MapImage = require('../../assets/map.png');

export function numberFormat(amount) {
  let cArray = amount.toString().split('');
  let cNumber = parseInt(amount.toString());

  if (cNumber <= 0) return '0';
  if (cNumber < 1000) return cNumber;

  let res = '';
  for (let i = 1; i <= cArray.length; i++) {
    res = cArray[cArray.length - i] + res;
    if (i % 3 == 0 && i != cArray.length) {
      res = ',' + res;
    }
  }

  return res;
}

class CovidData {
  constructor({
    cases = 0,
    todayCases = 0,
    deaths = 0,
    todayDeaths = 0,
    recovered = 0,
    todayRecovered = 0,
    active = 0,
    critical = 0,
    tests = 0,
    confirmed = 0,
  }) {
    (this.cases = numberFormat(cases)),
      (this.todayCases = numberFormat(todayCases)),
      (this.deaths = numberFormat(deaths)),
      (this.todayDeaths = numberFormat(todayDeaths)),
      (this.recovered = numberFormat(recovered)),
      (this.todayRecovered = numberFormat(todayRecovered)),
      (this.active = numberFormat(active)),
      (this.critical = numberFormat(critical)),
      (this.tests = numberFormat(tests)),
      (this.confirmed = numberFormat(confirmed));
  }
}

const Statistics = ({navigation, router}) => {
  const [worldData, setWorldData] = useState(new CovidData({}));
  const [localData, setLocalData] = useState(new CovidData({}));
  const [isGlobal, setGlobal] = useState(true);
  const [affectted, setAffected] = useState(0);
  const [death, setDeath] = useState(0);
  const [recoverd, setRecovered] = useState(0);
  const [active, setActive] = useState(0);
  const [serious, setSerious] = useState(0);
  const [specific, setSpecific] = useState('total');

  const getTotalData = async () => {
    // console.log('Covid data', await getTotalCovidData());
    const {worldStats, data} = await getTotalCovidData();
    setWorldData(new CovidData(worldStats)),
      setLocalData(new CovidData(data[0]));
    assineData();
  };

  const setState = ({affectted, death, recovered, active, serious}) => {
    setAffected(affectted);
    setDeath(death);
    setRecovered(recovered);
    setActive(active);
    setSerious(serious);
  };

  assineData = () => {
    if (isGlobal && specific === 'total') {
      setState({
        active: worldData.active,
        affectted: worldData.cases,
        death: worldData.deaths,
        serious: worldData.critical,
        recovered: worldData.recovered,
      });
    } else if (!isGlobal && specific === 'today') {
      setState({
        active: localData.active,
        affectted: localData.todayCases,
        death: localData.todayDeaths,
        serious: localData.critical,
        recovered: localData.todayRecovered,
      });
    } else if (isGlobal && specific === 'today') {
      setState({
        active: worldData.active,
        affectted: worldData.todayCases,
        death: worldData.todayDeaths,
        serious: worldData.critical,
        recovered: worldData.recovered,
      });
    } else {
      setState({
        active: localData.active,
        affectted: localData.cases,
        death: localData.deaths,
        serious: localData.critical,
        recovered: localData.recovered,
      });
    }
  };

  useEffect(() => {
    if (worldData.active == 0) getTotalData();
    assineData();
  }, [isGlobal, specific, worldData, localData]);

  const onChangeCountry = (isGlobal = false) => {
    setGlobal(isGlobal);
  };

  const onChangeSpecific = (specific = 'total') => {
    setSpecific(specific);
  };

  return (
    <NavContainer navigation={navigation}>
      <AppContainer>
        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <Button
            title="Myanmar"
            buttonStyle={{
              marginRight: 10,
              backgroundColor: !isGlobal
                ? COLOR.BackgroundColor
                : COLOR.PrimaryColor,
            }}
            textStyle={{
              color: !isGlobal ? COLOR.TextColorGray : COLOR.TextColor,
            }}
            onPress={() => onChangeCountry(false)}
          />
          <Button
            title="Global"
            buttonStyle={{
              marginLeft: 10,
              backgroundColor: isGlobal
                ? COLOR.BackgroundColor
                : COLOR.PrimaryColor,
            }}
            textStyle={{
              color: isGlobal ? COLOR.TextColorGray : COLOR.TextColor,
            }}
            onPress={() => onChangeCountry(true)}
          />
        </View>
        <View style={{height: 60, padding: 10, flexDirection: 'row'}}>
          <Container>
            <LinkButton
              title="Total"
              isSmall
              onPress={() => onChangeSpecific('total')}
            />
          </Container>
          <Container>
            <LinkButton
              title="Today"
              isSmall
              onPress={() => onChangeSpecific('today')}
            />
          </Container>
          {/* <Container>
            <LinkButton
              title="Yesterday"
              isSmall
              onPress={() => onChangeSpecific('yesterday')}
            />
          </Container> */}
        </View>
        <RowContainer>
          <ColorBox
            backgroundColor={COLOR.WarningColor}
            title="Affected"
            text={affectted}
          />

          <ColorBox
            backgroundColor={COLOR.SuccessColor}
            title="Recovered"
            text={recoverd}
          />
        </RowContainer>

        <RowContainer>
          <ColorBox
            backgroundColor={COLOR.DangerColor}
            title="Death"
            text={death}
          />

          <ColorBox
            backgroundColor={COLOR.InfoColor}
            title="Active"
            text={active}
          />

          <ColorBox
            backgroundColor={COLOR.SeriousColor}
            title="Serious"
            text={serious}
          />
        </RowContainer>
        <Container
          style={{
            flex: 2,
            backgroundColor: COLOR.BackgroundColor,
            borderRadius: 20,
            // alignItems: 'flex-start',
            paddingTop: 10,
          }}>
          <SubTitleText>Spread of virus</SubTitleText>
          <Container>
            <Image
              source={MapImage}
              style={{width: Dimensions.get('screen').width - 100, height: 110}}
            />
          </Container>
        </Container>
      </AppContainer>
    </NavContainer>
  );
};

export default Statistics;
