import React, {useState} from 'react';
import {AppContainer} from '../components/container/Container';
import {TextBox} from '../TextBox/TextBox';
import {Button, LinkButton} from '../components/Button/Button';
import News from '../Model/News';
import {TitleText, MainTitleText} from '../components/Typography/Typography';

export const AddNews = ({navigation}) => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [imageUrl, setImageUrl] = useState('');

  return (
    <AppContainer>
      <MainTitleText style={{marginBottom: 20}} bold>
        News
      </MainTitleText>
      <TextBox
        multiline={true}
        numberOfLines={3}
        placeholder="Title"
        onChange={setTitle}
        style={{marginBottom: 20}}
      />
      <TextBox
        multiline={true}
        numberOfLines={3}
        placeholder="Description"
        onChange={setDescription}
        style={{marginBottom: 20}}
      />
      <TextBox
        multiline={true}
        numberOfLines={3}
        placeholder="Image Url"
        onChange={setImageUrl}
        style={{marginBottom: 20}}
      />

      <Button
        title="Add"
        onPress={async () => {
          const {message} = await News.setNews({description, imageUrl, title});
          alert(message);
        }}
      />

      <LinkButton title="Cancel" onPress={() => navigation.navigate('News')} />
    </AppContainer>
  );
};
