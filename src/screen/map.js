import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button,
  Alert,
  TextInput,
  StatusBar,
  Linking,
} from 'react-native';
import InAppBrowser from 'react-native-inappbrowser-reborn';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {COLOR} from '../theme/color';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class CoronaMap extends Component {
  constructor(props) {
    super(props);

    this.state = {
      url: 'http://mmcovid19.glitch.me/',
      statusBarStyle: 'dark-content',
    };
  }

  sleep(timeout) {
    return new Promise((resolve) => setTimeout(resolve, timeout));
  }

  async openLink() {
    const {url, statusBarStyle} = this.state;
    try {
      if (await InAppBrowser.isAvailable()) {
        // A delay to change the StatusBar when the browser is opened
        const animated = true;
        const delay = animated && Platform.OS === 'ios' ? 400 : 0;
        setTimeout(() => StatusBar.setBarStyle('light-content'), delay);
        const result = await InAppBrowser.open(url, {
          // iOS Properties
          dismissButtonStyle: 'cancel',
          preferredBarTintColor: '#453AA4',
          preferredControlTintColor: 'white',
          readerMode: false,
          animated,
          modalPresentationStyle: 'fullScreen',
          modalTransitionStyle: 'partialCurl',
          modalEnabled: true,
          enableBarCollapsing: false,
          // Android Properties
          showTitle: true,
          toolbarColor: COLOR.PrimaryColor,
          secondaryToolbarColor: 'black',
          enableUrlBarHiding: true,
          enableDefaultShare: true,
          forceCloseOnRedirection: false,
          // Specify full animation resource identifier(package:anim/name)
          // or only resource name(in case of animation bundled with app).
          animations: {
            startEnter: 'slide_in_right',
            startExit: 'slide_out_left',
            endEnter: 'slide_in_left',
            endExit: 'slide_out_right',
          },
          headers: {
            'my-custom-header': 'my custom header value',
          },
        });
        // A delay to show an alert when the browser is closed
      } else {
        Linking.openURL(url);
      }
    } catch (error) {
      Alert.alert(error.message);
    } finally {
      // Restore the previous StatusBar of the App
      StatusBar.setBarStyle(statusBarStyle);
    }
  }

  render() {
    return (
      <Button
        style={styles.mapButton}
        title="Touch here to see the where covid happened"
        onPress={() => this.openLink()}
      />
    );
  }
}

const styles = StyleSheet.create({
  mapButton: {
    backgroundColor: 'white',
    color: 'white',
    paddingTop: 20,
  },
});
